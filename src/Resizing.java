import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class Resizing {
    private static final int SIZE = 1500;
    private static final int STEP = 10;
    private static final String INPUT_FILE_PATH = "1.png";
    private static final String OUTPUT_FILE_PATH = "resized.png";

    public static void main(String[] args) {
        BufferedImage image = null;
        double scale;

        try {
            image = ImageIO.read(new File(INPUT_FILE_PATH));
            int width = image.getWidth();
            int height = image.getHeight();

            if (width > height) {

                while (width > SIZE) {
                    width -= width / STEP;
                    height -= height / STEP;
                    if (width < SIZE) {
                        scale = (double) width / SIZE;
                        width = SIZE;
                        height /= scale;
                    }
                    image = scaleByTenPercents(image, width, height);
                }
            } else {
                while (height > SIZE) {
                    width -= width / STEP;
                    height -= height / STEP;
                    if (height < SIZE) {
                        scale = (double) height / SIZE;
                        height = SIZE;
                        width /= scale;
                    }
                    image = scaleByTenPercents(image, width, height);
                }
            }
            ImageIO.write(image, "png", new File(OUTPUT_FILE_PATH));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static BufferedImage scaleByTenPercents(BufferedImage image, int width, int height) {

        BufferedImage scaled = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = scaled.createGraphics();
        graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        graphics.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);

        graphics.drawImage(image, 0, 0, width, height, null);
        graphics.dispose();

        return scaled;
    }
}
